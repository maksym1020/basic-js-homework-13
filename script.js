'use strict'
// Практичне завдання:
// Реалізувати перемикання вкладок (таби) на чистому Javascript.
//
// Технічні вимоги:
// - У папці tabs лежить розмітка для вкладок. Потрібно, щоб після натискання на вкладку відображався
// конкретний текст для потрібної вкладки. При цьому решта тексту повинна бути прихована. У коментарях
// зазначено, який текст має відображатися для якої вкладки.
//
// - Розмітку можна змінювати, додавати потрібні класи, ID, атрибути, теги.
// - Потрібно передбачити, що текст на вкладках може змінюватись, і що вкладки можуть додаватися та видалятися.
// При цьому потрібно, щоб функція, написана в джаваскрипті, через такі правки не переставала працювати.
//
//  Умови:
//  - При реалізації обов'язково використовуйте прийом делегування подій
//  (на весь скрипт обробник подій повинен бути один).

const tabList = document.querySelector('.tabs');


const tabsContentItemsArray = [...document.querySelectorAll('.tabs-content-item')];

for (let i = 0; i < tabsContentItemsArray.length; i++) {
    tabsContentItemsArray[i].dataset.tabContent = i;
}

tabsContentItemsArray.forEach((value) => value.style.display = 'none');

const tabsArray = [...document.querySelectorAll('.tabs-title')];

for (let i = 0; i < tabsArray.length; i++) {
    tabsArray[i].dataset.tab = i;
}
tabsArray.forEach((value) => {
    if (value.classList.contains('active')) {

        let contentId = value.dataset.tab;
        let activeTabContent = tabsContentItemsArray[contentId];

        activeTabContent.style.display = 'flex';
    }
});


tabList.addEventListener('click', onClickDisplayTab);

function onClickDisplayTab(event) {
    tabsArray.forEach((value) => value.classList.remove('active'));
    tabsContentItemsArray.forEach((value) => {
        value.style.display = 'none';
        if (value.dataset.tabContent === event.target.dataset.tab) {
            event.target.closest('li').classList.add('active');
            value.style.display = 'flex';
        }
    })
}